---
author: #module_leader
date: #date
documentclass: hitec
fontfamily: opensans
fontsize: 12
...
# Overview


The final report brings together all of the previous deliverables into
a coherent report documenting the answer(s) to your research question.

# Instructions


#.  Read Section 6.5 "Data Synthesis" of Kitchenham &
    Charters' "Guidelines for performing systematic literature reviews
    in software engineering".

#.  Clone your group's Bitbucket repository.

#.  Copy _final_report.docx_ from this repository to your group workspace.

#.  Edit the report heading in _final_report.docx_:

    #. Replace "Paper Title" with the title of your report.  It should
    reflect the topic and research question.
    #. Replace "NN" with your group number.
    #. Replace "group member names (without IDs please)" with the name
    of *each* group member, _as it appears on Canvas._
    #. Write an introduction to your review.  

#.  Write an _Introduction_ section for your report, explaining:

    #. the _context_ in which your research question should be
    interpreted, 
    #. your _research question_, and
    #. the _motivation_ for your research question (with citations).

#.  Write a _Review Protocol_ section for your report, documenting:

    #. the "_PICO_" (population, intervention, comparison, and  outcome)
     elements for your review, 
    #. your _search string_, and
    #. your _inclusion_ and _exclusion_ criteria.

#. Write a _Results_ section for your report, documenting:

    #. the number of papers identified by your search string in the
     initial search,
    #. the number of papers that passed the "include title" step,
    #. the number of papers that passed the "include abstract" step,
    #. the number of papers that passed the "exlude" step (this should
     be the number of papers with with "Yes" in the "accept contents"
     column).
     
#. Write a _Synthesis_ section for your report, including:

    #. a table listing all _codes_ used in the coding step, with an example quotation for each;
    #. a table of _themes_, with the codes belonging to each theme.
    #. a table of _model themes_, with *all* quotations that are grouped under each
    theme, and proper  citations to the source of the quotation.

    If you don't know what these terms mean, review the Synthesis
    Tutorial and slides on Canvas (under the "Ethics" unit).
 
#. Write a _Conclusion_ section for your report, comprising a paragraph that states the
    one or two big things you learned from reading all these papers,
    with proper citations to the supporting literature.

#. Write a _References_ section for your report, comprising _two_
 reference lists:
 
    1. A list of properly formatted references cited as supporting
    evidence, mostly in the Introduction but possibly also in the
    Synthesis and Conclusion.
    2. A list of properly formatted references for *all* papers
    accepted for review.
     

#.  Proofread, then commit your revised _final_report.docx_ file.  You
    are _strongly_ advised to submit your report to _grammarly.com_ to
    help identify grammar, punctuation, and spelling mistakes.

#.  Push your workspace to Bitbucket before midnight #cw_report_due.

#. Create a *PDF* version of your report and submit it via the "Final
   Report Submission" page on Canvas by midnight  #cw_report_due.
